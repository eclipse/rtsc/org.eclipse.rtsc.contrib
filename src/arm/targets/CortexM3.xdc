/*!
 *  ======== CortexM3 ========
 *  Experimental Cortex M3 target
 */
metaonly module CortexM3 inherits ITarget
{
    override readonly config string name    = "CortexM3";

    override readonly config string suffix  = "em3";

    override readonly config string isa     = "v7M";

    override readonly config xdc.bld.ITarget.Model model= {
	endian: "little"
    };

    /*
     *  The command used to compile C/C++ source files into object files
     *  @p(dlist)
     *      - `--depend`
     *          this option is required to generate header file dependencies.
     *          It can be removed but source files will not be remade when
     *          headers change.
     *
     *      - `--depend-format=unix`
     *          this option ensures that '/' is used even on Windows hosts
     *          since '/' is a legit directory specifier this should result
     *          in more portable dependencies.
     *
     *      - `--littleend`
     *          force little endian compile implied by this target
     *
     *      - `--cpu=Cortex-M3`
     *          Specify architecture.
     */
    override readonly config arm.targets.ITarget.Command cc = {
        cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armcc",
        opts: "-c --depend=$@.dep --depend-format=unix --littleend --cpu=Cortex-M3"
    };

    /*
     *  The command used to assemble assembly source files into object files
     *  @p(dlist)
     *      - `--littleend`
     *          force little endian compile implied by this target
     *
     *      - `--cpu=Cortex-M3`
     *          Specify architecture.
     */
    override readonly config arm.targets.ITarget.Command asm = {
        cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armasm",
        opts: "--littleend --cpu=Cortex-M3"
    };
}
