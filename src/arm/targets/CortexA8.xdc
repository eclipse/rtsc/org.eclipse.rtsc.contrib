/*!
 *  ======== CortexA8 ========
 *  Experimental Cortex A8 target
 */
metaonly module CortexA8 inherits ITarget
{
    override readonly config string name    = "CortexA8";

    override readonly config string suffix  = "ea8";

    override readonly config string isa     = "v7A";

    override readonly config xdc.bld.ITarget.Model model= {
	endian: "little"
    };

    /*
     *  The command used to compile C/C++ source files into object files
     *  @p(dlist)
     *      - `--depend`
     *          this option is required to generate header file dependencies.
     *          It can be removed but source files will not be remade when
     *          headers change.
     *
     *      - `--depend-format=unix`
     *          this option ensures that '/' is used even on Windows hosts
     *          since '/' is a legit directory specifier this should result
     *          in more portable dependencies.
     *
     *      - `--littleend`
     *          force little endian compile implied by this target
     *
     *      - `--cpu=Cortex-A8`
     *          Specify architecture.
     *
     *      - `--fpu=vfpv3`
     *          Specify FPU.
     */
    override readonly config arm.targets.ITarget.Command cc = {
        cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armcc",
        opts: "-c --depend=$@.dep --depend-format=unix --littleend --cpu=Cortex-A8 --fpu=vfpv3"
    };

    /*
     *  The command used to assemble assembly source files into object files
     *  @p(dlist)
     *      - `--littleend`
     *          force little endian compile implied by this target
     *
     *      - `--cpu=Cortex-A8`
     *          Specify architecture.
     *
     *      - `--fpu=vfpv3`
     *          Specify FPU.
     */
    override readonly config arm.targets.ITarget.Command asm = {
        cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armasm",
        opts: "--littleend --cpu=Cortex-A8 --fpu=vfpv3"
    };
}
