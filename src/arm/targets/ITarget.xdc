/*!
 *  ======== ITarget ========
 *  Common target definitions for all RVCT Arm tools
 *
 *  This interface provides base definitions for targets that wrap
 *  the RVCT compiler tools from Arm Ltd.
 *
 *  These targets require configuration of more than just `rootDir` because
 *  the physical design of the RVCT compiler seems to always place the
 *  compiler executables and the headers and libraries for that compiler in
 *  directores that are separated by directories that contain the compiler
 *  version.  Rather than keeping all these directores under a single version
 *  directory each is in a separate directory containing a version number.
 *  As a result, you must supply additional configuration settings to use
 *  the RVCT compiler.
 *
 *  These targets assume that libraries and headers and the compiler are
 *  all under a directories that share the same version number.  For example,
 *  The compiler is in a directory of the form `Programs/VERS/build/targ`
 *  and the headers are in a directory of the form `Data/VERS/include`,
 *  where `VERS` is something like "`4.0/400`".
 *
 *  @a(Example)
 *  The following fragment added to a `config.bld` script will enable the
 *  user to build for the Cortex A8 using the 4.0/400 RVCT tools for Linux
 *  which are installed in `/home/user/RVCT`.
 *  @p(code)
 *      var CortexA8 = xdc.module("arm.targets.CortexA8");
 *      CortexA8.rootDir = "/home/user/RVCT";
 *      CortexA8.VERS = "4.0/400";
 *      CortexA8.TARG = "linux-pentium";
 * @p
 */
@TargetHeader("xdc/bld/stddefs.xdt")
metaonly interface ITarget inherits xdc.bld.ITarget
{
    override readonly config string rts         = "arm.targets.rts";
    override config string platform             = "xdc.platform.generic";
    override readonly config string stdInclude  = "arm/targets/std.h";

    /*!
     *  ======== version ========
     *  The Compatibility Key associated with this target.
     *
     *  The first two components of this package's Compatibility Key are '1,0'.
     *  The rest of the Key represents the compiler version. The third
     *  component combines the major and the minor version number in the format
     *  Major.Minor. The fourth component is the build number.
     *
     *  @a(Example)
     *  If this target's `rootDir` points to the compiler version 3.4.6, the 
     *  Compatibility Key is [1,0,3.4,6].
     *  
     */
    override metaonly config String version;

    /*
     *  ======== profiles ========
     */
    override config xdc.bld.ITarget.OptionSet profiles[string] = [
	["debug", {
	    compileOpts: {
		copts: "-g",
		defs:  "-D_DEBUG_=1",
	    },
	    linkOpts: "-g",
	}],

	["release", {
	    compileOpts: {
		copts: "-O2",
	    },
	    linkOpts: "",
	}],
    ];

    /*!
     *  ======== versionMap ========
     *  Map of Arm compiler version numbers to compatibility keys.
     *
     *  This map translates version string information from the compiler
     *  into a compatibility key.  The compatibilty key is used to
     *  validate consistency among a collection of packages used in
     *  a configuration.
     *
     *  The compiler version string is "arm<ver>", where <ver> is
     *  VERS.
     *
     *  If a compiler version is not found in this map the default is
     *  "1,0,<ver>", where <ver> is the compiler version number.  Thus,
     *  the user only needs to extend this table when a significant
     *  incompatibility occurs or when two versions of the compiler should
     *  be treated as 100% compatible.
     *
     *  @see #VERS
     */
    override config string versionMap[string] = [
	["arm4.0", "1,0,4.0,0"],
    ];

    /*!
     *  ======== Command ========
     *  Required command and options.
     *
     *  The compile, link, and archive functions in this interface are
     *  implemented by expanding the strings specified in this structure
     *  and inserting strings from the `Options` structure to form a single
     *  command.  The strings in this structure can not be changed by
     *  the user (they are fixed by the target), but the string in the
     *  `Options` structure may be changed by the user.
     *
     *  The final command is:
     *  @p(code)
     *	    Command.cmd Options.prefix Command.opts Options.suffix
     *  @p
     *
     *  @field(cmd)	name of a tool-chain executable without any path
     *			information.  The location of this executable is
     *			specified by the binDir (or pathPrefix) 
     *			configuration parameter.
     *
     *	@field(opts)	required options passed to the command; these options
     *			can not be changed or eliminated by user's
     *			configuration script.
     */
    struct Command {
	string cmd;	/*! the command to run */
	string opts;	/*! required options for the command */
    }

    /*!
     *  ======== Options ========
     *  User configurable command options.
     *
     *  The option strings allow the user to pass additional parameters to the
     *  executable that is responsible for compiling, linker, or archiving.
     */
    struct Options {
	string prefix;	/*! options that appear before Command.opts */
	string suffix;	/*! options that appear after Command.opts */
    };

    /*
     *  ======== LONGNAME ========
     *  Path to the compiler
     *
     *  This parameter is used only if it is necessary to compute
     *  `VERS` by running the compiler; i.e., if `VERS` is either not
     *  set or set to `null` in the users `config.bld` script.
     *
     *  This string is a path relative to `rootDir` that names the
     *  ARM compiler.  For example:
     *  @p(code)
     *      CortexA8.LONGNAME = "Programs/4.0/400/linux-pentium/armcc";
     *  @p
     *
     *  @see #VERS
     *  @see #TARG
     */
    config String LONGNAME;
    
    /*!
     *  ======== VERS ========
     *  A compiler version string
     *
     *  This string is used to compose the path to a specific version
     *  of the compiler.  For example, "4.0/400" denotes build number 400
     *  of the 4.0 version of the compiler.
     *
     *  The RVCT tools use a convention of placing the compiler in a
     *  sub-directory for the form:
     *  @p(code)
     *      major.minor/build/targ
     *  @p
     *  where `major.minor` is the version reported by the compiler,
     *  `build` is the build or patch number (also reported by the compier),
     *  and `targ` is a "target name" that identifies the host architecture
     *  that runs the compiler.  `targ` is specified by the user via the
     *  {@link #TARG} configuration parameter.
     *
     *  If `VERS` is not set or set to `null`, {@link #LONGNAME} is used
     *  to invoke the compiler to get the value of `VERS` from the compiler
     *  itself.
     *
     *  @see #LONGNAME
     *  @see #TARG
     */
    config String VERS;

    /*!
     *  ======== TARG ========
     *  The host running the compiler
     *
     *  This string identifies the host architecture for which the compiler
     *  itself was built.  This name is also the name of the directory
     *  containing the compiler.
     *
     *  For Windows systems you must set this parameter to "win_32-pentium".
     *
     *  @see #VERS
     */
    config String TARG = "linux-pentium";
    
    /*!
     *  ======== ar ========
     *  The command used to create an archive
     */
    readonly config Command ar = {
	cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armar",
	opts: "--create -r"
    };

    /*!
     *  ======== arOpts ========
     *  User configurable archiver options.
     */
    config Options arOpts = {
	prefix: "",
	suffix: ""
    };

    /*!
     *  ======== lnk ========
     *  The command used to link executables.
     */
    readonly config Command lnk = {
	cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armlink",
	opts: ""
    };

    /*!
     *  ======== lnkOpts ========
     *  User configurable linker options.
     */
    config Options lnkOpts = {
	prefix: "--list $(XDCCFGDIR)/$@.map --map --info=libraries,summarysizes,summarystack",
	suffix: "--libpath $(rootDir)/Data/$(VERS)/lib"
    };

    /*!
     *  ======== cc ========
     *  The command used to compile C/C++ source files into object files
     *  @p(dlist)
     *      - `--depend`
     *          this option is required to generate header file dependencies.
     *          It can be removed but source files will not be remade when
     *          headers change.
     *
     *      - `--depend-format=unix`
     *          this option ensures that '/' is used even on Windows hosts
     *          since '/' is a legit directory specifier this should result
     *          in more portable dependencies.
     */
    readonly config Command cc = {
        cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armcc",
        opts: "-c --depend=$@.dep --depend-format=unix"
    };

    /*!
     *  ======== ccOpts ========
     *  User configurable compiler options.
     */
    config Options ccOpts = {
	prefix: "",
	suffix: "-Dfar="
    };

    /*!
     *  ======== ccConfigOpts ========
     *  User configurable compiler options for the generated config C file.
     *
     *  By default, this parameter inherits values specified in ccOpts, by
     *  expanding $(ccOpts.prefix) and $(ccOpts.suffix) into the values
     *  specified in ccOpts for this target.
     *
     *  By default, we disable the following diagnostics for this file:
     *  @dlist
     *      - C111
     *          suppress '`statement is unreachable`' warnings that
     *          come from generated code that contains if statments
     *          that the compiler can evaluate, resulting in early returns
     *          from a function which the compiler then warns about(!).
     *
     *      - C1207
     *          if --gnu is used, this tag suppresses the
     *          '`attribute "externally_visible" ignored`' warnings
     *
     *      - C1296
     *          this tag suppresses 
     *          '`extended constant initialiser used`' warnings from casts
     *          that are required for GCC compilers
     */
    config Options ccConfigOpts = {
	prefix: "$(ccOpts.prefix) --diag_suppress=C111,C1207,C1296",
	suffix: "$(ccOpts.suffix)"
    };

    /*!
     *  ======== asm ========
     *  The command used to assemble assembly source files into object files
     */
    readonly config Command asm = {
	cmd: "$(rootDir)/Programs/$(VERS)/$(TARG)/armasm",
	opts: ""
    };

    /*!
     *  ======== asmOpts ========
     *  User configurable assembler options.
     */
    config Options asmOpts = {
	prefix: "",
	suffix: ""
    };

    /*!
     *  ======== includeOpts ========
     *  Additional user configurable target-specific include path options
     */
    config string includeOpts = "-I $(rootDir)/Data/$(VERS)/include/unix";

    /*
     *  ======== stdTypes ========
     */
    override readonly config xdc.bld.ITarget.StdTypes stdTypes = {
	t_IArg		: { size: 4, align: 4 },
	t_Char		: { size: 1, align: 1 },
	t_Double	: { size: 8, align: 8 },
	t_Float		: { size: 4, align: 4 },
	t_Fxn		: { size: 4, align: 4 },
	t_Int		: { size: 4, align: 4 },
	t_Int8		: { size: 1, align: 1 },
	t_Int16		: { size: 2, align: 2 },
	t_Int32		: { size: 4, align: 4 },
	t_Int64		: { size: 8, align: 4 },
	t_Long		: { size: 4, align: 4 },
	t_LDouble	: { size: 8, align: 8 },
	t_LLong		: { size: 8, align: 8 },
	t_Ptr		: { size: 4, align: 4 },
	t_Short		: { size: 2, align: 2 },
    };
}
