XDCROOT = $(TOOLS)/vendors/xdc/xdctools_3_15_00_50/$(BUILD_HOST_OS)

all:
	@$(XDCROOT)/xdc release -PR src

%:
	@$(XDCROOT)/xdc $@ -PR src
